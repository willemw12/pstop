﻿# Pause or resume background processes or CLI programs

Temporarily stop running processes.

Mainly intended for background processes and CLI programs.
A GUI program may fail to resume, for example, with error "Lost connection to Wayland compositor".


## Feature

- Pause, resume, display the status of processes (by name or by PID number)


## Dependencies

- bash
- pgrep (package procps or procps-ng)


## Usage

For usage information, run:

    ./pstop --help


## License

GPL-3.0-or-later


## Link

[GitLab](https://gitlab.com/willemw12/pstop)
